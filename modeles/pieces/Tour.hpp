#pragma once
#include "Piece.hpp"

class Tour : public Piece {
    public:
        Tour(bool estNoir);
        string getLettre();
        bool peutSeDeplacer(const Position& depuis, const Position& vers);
};
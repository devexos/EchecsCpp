#include "Pion.hpp"

Pion::Pion(bool estNoir) : Piece(estNoir) {}

string Pion::getLettre() {
    if (this->estNoir()) {
        return "P";
    }
    return "p";
}

bool Pion::peutSeDeplacer(const Position& depuis, const Position& vers) {
    if (depuis.colonne != vers.colonne)
        return false;
    int deltaLigne = depuis.ligne - vers.ligne;
    if (this->estNoir()) {
        deltaLigne = -deltaLigne;
    }
    if (deltaLigne != -1 && deltaLigne != -2) {
        return false;
    }
    return true;
}

bool Pion::peutSeDeplacerPourPrendre(const Position& depuis, const Position& vers) {
    int deltaLigne = depuis.ligne - vers.ligne;
    int deltaColonne = depuis.colonne - vers.colonne;
    if (deltaColonne != 1 && deltaColonne != -1) {
        return false;
    }
    if (this->estNoir()) {
        deltaLigne = -deltaLigne;
    }
    return deltaLigne == -1;
    
}

#include "Cavalier.hpp"

Cavalier::Cavalier(bool estNoir) : Piece(estNoir) {}

string Cavalier::getLettre() {
    if (this->estNoir()) {
       return "C";
    }
    return "c";
}

bool Cavalier::peutSeDeplacer(const Position& depuis, const Position& vers) {
    int deltaLigne = depuis.ligne - vers.ligne;
    int deltaColonne = depuis.colonne - vers.colonne;
    int produitDelta = deltaLigne * deltaColonne;

    return produitDelta == 2 || produitDelta == -2;  
}

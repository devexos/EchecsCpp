#pragma once 
#include <string>
#include "../Position.hpp"

using namespace std;

class Piece {
    bool _estNoir;
    public:
        Piece(bool estNoir) : _estNoir(estNoir) {}
        bool estNoir() { return _estNoir; }
        virtual string getLettre() = 0;
        virtual bool peutSeDeplacer(const Position& depuis, const Position& vers) = 0;
        virtual bool peutSeDeplacerPourPrendre(const Position& depuis, const Position& vers)  {
            return this->peutSeDeplacer(depuis, vers);
        }
};
#include "Tour.hpp"

Tour::Tour(bool estNoir) : Piece(estNoir) {}

string Tour::getLettre() {
    if (this->estNoir()) {
       return "T";
    }
    return "t";
}

bool Tour::peutSeDeplacer(const Position& depuis, const Position& vers) {
    int deltaLigne = depuis.ligne - vers.ligne;
    int deltaColonne = depuis.colonne - vers.colonne;

    return (deltaLigne == 0 && deltaColonne != 0) || (deltaLigne != 0 && deltaColonne == 0);  
}

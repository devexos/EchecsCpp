#pragma once
#include "Piece.hpp"

class Pion : public Piece {

    public:
        Pion(bool estNoir);
        string getLettre();
        bool peutSeDeplacer(const Position& depuis, const Position& vers);
        bool peutSeDeplacerPourPrendre(const Position& depuis, const Position& vers);
};
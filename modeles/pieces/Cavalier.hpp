#pragma once
#include "Piece.hpp"

class Cavalier : public Piece {
    public:
        Cavalier(bool estNoir);
        string getLettre();
        bool peutSeDeplacer(const Position& depuis, const Position& vers);
};
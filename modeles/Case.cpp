#include "Case.hpp"

Case::Case() {}

Case::~Case() {
    if (this->_piece != nullptr)
        delete this->_piece;
}

string Case::getNomDeLaPiece() const {
    if (this->_piece == nullptr)
        return "";
    return this->_piece->getLettre();
}

Piece* Case::getPiece() const {
    return this->_piece;
}

void Case::setPiece(Piece* piece) {
    if (this->_piece != nullptr)
        delete this->_piece;
    this->_piece = piece;
}


bool Case::estVide() const {
    return this->_piece == nullptr;
}

Piece* Case::retirePiece() {
    Piece * piece = this->_piece;
    this->_piece = nullptr;
    return piece;
}
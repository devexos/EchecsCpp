#pragma once
#include "Case.hpp"
#include "Position.hpp"
#include "pieces/Pion.hpp"
#include "pieces/Cavalier.hpp"
#include "pieces/Tour.hpp"

class Jeu {
    Case _cases[8][8];
    bool _tourEstAuxNoirs = false;
    public:
        Jeu();
        bool getTourEstAuxNoirs() const;
        void initialisationDesPieces();
        const Case& getCase(int colonne, int ligne) const;
        const Case& getCase(const Position& position) const;
        Case& getCaseEditable(const Position& position);
        void joue(const Position& depuis, const Position& vers);
};
#pragma once
#include <string>
#include "pieces/Piece.hpp"

using namespace std;

class Case {
    Piece* _piece = nullptr;
    public:
        Case();
        ~Case();

        string getNomDeLaPiece() const;
        Piece* getPiece() const;
        void setPiece(Piece* pion);
        bool estVide() const;
        Piece* retirePiece();
};
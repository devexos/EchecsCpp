#include "Jeu.hpp"

Jeu::Jeu() {
    this->initialisationDesPieces();
}

bool Jeu::getTourEstAuxNoirs() const {
    return this->_tourEstAuxNoirs;
}

void Jeu::initialisationDesPieces() {
    for(int colonne = 0; colonne < 8; colonne++) {
        _cases[colonne][1].setPiece(new Pion(false));
        _cases[colonne][6].setPiece(new Pion(true));
    }
    _cases[1][0].setPiece(new Cavalier(false));
    _cases[1][7].setPiece(new Cavalier(true));
    _cases[6][0].setPiece(new Cavalier(false));
    _cases[6][7].setPiece(new Cavalier(true));

    _cases[0][0].setPiece(new Tour(false));
    _cases[0][7].setPiece(new Tour(true));
    _cases[7][0].setPiece(new Tour(false));
    _cases[7][7].setPiece(new Tour(true));

}

const Case& Jeu::getCase(int colonne, int ligne)  const {
    return this->_cases[colonne][ligne];
}

const Case& Jeu::getCase(const Position& position) const {
    return this->getCase(position.colonne, position.ligne);
}

Case& Jeu::getCaseEditable(const Position& position) {
    return this->_cases[position.colonne][position.ligne];
}


void Jeu::joue(const Position& depuis, const Position& vers) {
    if (this->getCase(depuis).estVide())
        return;
    Piece* piece = this->getCase(depuis).getPiece();
    if (piece->estNoir() != _tourEstAuxNoirs) {
        return;
    }
    if (!this->getCase(vers).estVide()) {
        if (this->getCase(vers).getPiece()->estNoir() == _tourEstAuxNoirs) {
            return;
        }
        if (!piece->peutSeDeplacerPourPrendre(depuis, vers)) {
            return;
        }
    } else {
        if (!piece->peutSeDeplacer(depuis, vers)) {
            return;
        }
    }
    piece = this->getCaseEditable(depuis).retirePiece();
    this->getCaseEditable(vers).setPiece(piece);
    _tourEstAuxNoirs = !_tourEstAuxNoirs;
}
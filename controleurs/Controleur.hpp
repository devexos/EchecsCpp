#pragma once
#include "../modeles/Jeu.hpp"
#include "../vues/Vue.hpp"

class Controleur {
    Jeu _jeu;
    Vue _vue;

    public: 
        Controleur() : _vue(Vue(_jeu)) {}
        void affichageInitial() {
            _vue.afficherCases();
            _vue.afficherAQuiLeTour();
        }
        void joue(Position depuis, Position vers) {
            _jeu.joue(depuis, vers);
            _vue.afficherDeplacement(depuis, vers);
            _vue.afficherCases();
            _vue.afficherAQuiLeTour();
        } 
};
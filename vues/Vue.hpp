#pragma once
#include <iostream>
#include "../modeles/Jeu.hpp"
#include "../modeles/Position.hpp"
using namespace std;

class Vue {
    const Jeu& _jeu;
    public: 
        Vue(const Jeu& jeu);
        void afficherCases();
        void afficherDeplacement(const Position& depuis, const Position& vers);
        void afficherAQuiLeTour();
};
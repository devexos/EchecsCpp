#include "Vue.hpp"

Vue::Vue(const Jeu& jeu) : _jeu(jeu) {}

void Vue::afficherCases() {
    for(int ligne = 7; ligne >= 0; ligne--) {
        for(int colonne = 0; colonne < 8; colonne++) {
            if (_jeu.getCase(colonne, ligne).estVide())
                cout << "_ ";
            else
                cout << _jeu.getCase(colonne, ligne).getNomDeLaPiece() << " ";
        }
        cout << endl;
    }
}
void Vue::afficherDeplacement(const Position& depuis, const Position& vers) {
    cout << "-------------------" << endl;
    cout << "Déplacement de (" << depuis.colonne << "," << depuis.ligne << ") ";
    cout << "vers (" << vers.colonne << "," << vers.ligne << ") " << endl;
    cout << "-------------------" << endl;
}

void Vue::afficherAQuiLeTour() {
    if (_jeu.getTourEstAuxNoirs()) {
        cout << "C'est aux noirs de jouer" << endl;
    } else {
        cout << "C'est aux blancs de jouer" << endl;
    }
}
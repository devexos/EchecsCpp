#include "controleurs/Controleur.hpp"
#include "modeles/Position.hpp"

int main() {

    Controleur controleur;
    controleur.affichageInitial();
    controleur.joue(Position(2,1), Position(2,4));
    controleur.joue(Position(2,1), Position(2,3));
    controleur.joue(Position(1,1), Position(1,2));
    controleur.joue(Position(1,6), Position(1,5));
    controleur.joue(Position(1,0), Position(1,4));
    controleur.joue(Position(1,0), Position(0,2));
    controleur.joue(Position(0,7), Position(4,4));
    controleur.joue(Position(0,7), Position(0,2));
    controleur.joue(Position(0,1), Position(0,2));
    controleur.joue(Position(1,1), Position(0,2));
    return 0;
}